
import java.util.Arrays;
import java.util.Random;

class Solution {

    public static void main(String[] args) {
        int [] a  = new int[]{1,2,3};
        Solution solution = new Solution(a);
        a = solution.shuffle();
        for (int i : a) {
            System.out.print(i);
        }
        solution.reset();
    }

    private int[] nums;
    private Random random;

    public Solution(int[] nums) {
        this.nums = nums;
        random = new Random();
    }

    /** Resets the array to its original configuration and return it. */
    public int[] reset() {
        return nums;
    }

    /** Returns a random shuffling of the array. */
    public int[] shuffle() {
        if (nums.length==0)
            return null;
        int [] result = Arrays.copyOf(nums,nums.length);
        for (int i = 0; i < result.length; i++) {
            helper(result, i, i+random.nextInt(result.length-i));
        }
        return result;
    }

    private void helper(int[] result, int i, int j) {
        int temp = result[i];
        result[i] = result[j];
        result[j] = temp;
    }
}